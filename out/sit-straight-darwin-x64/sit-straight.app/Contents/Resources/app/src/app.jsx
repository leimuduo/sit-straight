import React, { useRef, useState, useCallback } from 'react';
import Webcam from 'react-webcam';
import { withStateHandlers, lifecycle, compose } from 'recompose';
import * as posenet from '@tensorflow-models/posenet';
import fp from 'lodash/fp';
import { Button, Progress, Checkbox } from 'semantic-ui-react'

function convertURIToImageData(URI) {
  return new Promise(function(resolve, reject) {
    if (URI == null) return reject();
    var canvas = document.createElement('canvas'),
        context = canvas.getContext('2d'),
        image = new Image();
    image.addEventListener('load', function() {
      canvas.width = image.width;
      canvas.height = image.height;
      context.drawImage(image, 0, 0, canvas.width, canvas.height);
      resolve(context.getImageData(0, 0, canvas.width, canvas.height));
    }, false);
    image.src = URI;
  });
}

const getDistance = (from, to) => {
  const xDiff = from.x - to.x
  const yDiff = from.y - to.y
  return Math.sqrt((xDiff * xDiff) + (yDiff * yDiff))
}

const getMidPos = (from, to) => {
  return {
    x: (from.x + to.x)/2,
    y: (from.y + to.y)/2
  }
}

async function estimate(image, net) {
  if (net) {
    const pose = await net.estimateSinglePose(image, {
      flipHorizontal: false
    })
    if (pose) {
      const nose = fp.find(
        ({part}) => part === 'nose'
      )(pose.keypoints)
      const leftShoulder = fp.find(
        ({part}) => part === 'leftShoulder'
      )(pose.keypoints)
      const rightShoulder = fp.find(
        ({part}) => part === 'rightShoulder'
      )(pose.keypoints)
      if (nose && leftShoulder && rightShoulder) {
        const shoulderWidth = getDistance(leftShoulder.position, rightShoulder.position)
        const leftHeight = getDistance(leftShoulder.position, nose.position)
        const rightHeight = getDistance(nose.position, rightShoulder.position)
        const mid = getMidPos(leftShoulder.position, rightShoulder.position)
        const height = getDistance(nose.position, mid)
        return height / shoulderWidth
      }
    }
  }
  return null
}

const App = ({net}) => {
  const webcamRef = useRef(null);
  const [intervalRef, setIntervalRef] = useState(undefined)
  const [shot, setShot] = useState(undefined)
  const [ratio, setRatio] = useState(undefined)

  const startWatching = useCallback(
    () => {
      clearInterval(intervalRef)
      const itv = setInterval(
        () => {
          if (webcamRef && webcamRef.current) {
            const imageSrc = webcamRef.current.getScreenshot();
            setShot(imageSrc)

            convertURIToImageData(imageSrc).then(function(imageData) {
              estimate(imageData, net).then(
                (ratio) => {
                  setRatio(ratio)
                }
              )
            });
          }
        },
        1000
      )
      setIntervalRef(itv)
    },
    [webcamRef, net]
  )

  const stopWatching = () => {
    clearInterval(intervalRef)
    setIntervalRef(undefined)
    setShot(undefined)
  }

  const [threshold, setThreshold] = useState(0.55)
  const increaseThreshold = () => {
    setThreshold(
      Math.min(1, threshold+0.02)
    )
  }
  const decreaseThreshold = () => {
    setThreshold(
      Math.max(0, threshold-0.02)
    )
  }

  const [showNotification, setShowNotification] = useState(true)
  const [playSound, setPlaySound] = useState(false)
  const [showWindow, setShowWindow] = useState(false)
  const toggleNotification = () => {
    setShowNotification(!showNotification)
  }
  const toggleSound = () => {
    setPlaySound(!playSound)
  }
  const toggleWindow = () => {
    setShowWindow(!showWindow)
  }

  const shouldSitup = ratio ?
    ratio < threshold :
    false

  if (shouldSitup) {
    if (showNotification) {
      let myNotification = new Notification('Sit straight', {
        body: 'You need to sit straight'
      })
    }

    if (showWindow) {
      const { remote } = require('electron')
      remote.getCurrentWindow().maximize();
      remote.getCurrentWindow().show();
    }
  }

  return (<div>
    <h1>Watch your back</h1>
    <p>As a software engineer I sit for like 8 hours a day at least, making it difficult for my back. This software watches you through the camera and remind you when you need to sit straight.</p>
    <div id='cam'>
      <Webcam
        audio={false}
        height={200}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        width={300}
      />
    </div>
    <div id='control'>
      {
        webcamRef && net ?
          intervalRef ?
            <Button onClick={stopWatching}>stop</Button> :
            <Button onClick={startWatching}>start</Button> :
          null
      }
      <div>
        <h3>Mark</h3>
        <Progress value={ratio} total={1} indicating />
      </div>
      <div>
        <h3>Strictness</h3>
        <Progress value={threshold} total={1} indicating />
        <Button onClick={increaseThreshold}>Increase</Button>
        <Button onClick={decreaseThreshold}>Decrease</Button>
      </div>
      <div>
        <h3>How do you want to be reminded?</h3>
        <Checkbox checked={showNotification} onClick={toggleNotification} label='notification' />
        {/* <Checkbox checked={playSound} onClick={toggleSound} label='sound' /> */}
        <Checkbox checked={showWindow} onClick={toggleWindow} label='screen' />
      </div>
    </div>
  </div>)
}

export default compose(
  withStateHandlers(
    {
      net: undefined,
    },
    {
      setNet: () => (net) => ({net})
    }
  ),
  lifecycle({
    componentDidMount(){
      posenet.load().then(
        (net) => {
          console.log('set net')
          this.props.setNet(net)
        }
      ).catch(
        (err) => {
          console.log(err)
        }
      )
    }
  })
)(App);